use std::io::{self, Write};
use rand::Rng;


struct DataParty {
    won: u32,
    lose: u32,
    tie: u32,
    parties: u32
}


fn show_stadistic(info_parties: DataParty) {
    if info_parties.parties <= 1 {
        println!("\nHaz jugado {}", info_parties.parties);
    } else {
        println!("\nHaz jugado {} partidas", info_parties.parties);
        println!("Haz ganado {}", info_parties.won);
        println!("Haz perdido {}", info_parties.lose);
        println!("Haz empetado {}", info_parties.tie);
    }
}


fn change_status(info_parties: &mut DataParty, result_party: i8) {
    info_parties.parties += 1;
    if result_party == 1 {
        info_parties.won += 1;
    } else if result_party == -1 {
        info_parties.lose +=1
    } else if result_party == 0 {
        info_parties.tie += 1;
    }
}


fn input_string(message: &str) -> String {
    print!("{}", message);
    io::stdout().flush().unwrap();
    let mut string_variable: String = String::new();
    io::stdin().read_line(&mut string_variable).expect("Failed to read line");
    let new_variable: String = string_variable.trim().to_string();
    new_variable
}


fn who_wins(first_player: String, second_player: String) -> i8 {
    let mut status_play: i8 = 0;  
    if  first_player.to_lowercase() == second_player.to_lowercase() {
        status_play = 0;
    } else if first_player.to_lowercase() ==  "tijera" && second_player.to_lowercase() == "papel" {
        status_play = 1;
    }  else if first_player.to_lowercase() == "tijera" && second_player.to_lowercase() == "piedra" {
        status_play = -1;
    } else if first_player.to_lowercase() ==  "piedra" && second_player.to_lowercase() == "papel" {
        status_play = -1;
    } else if first_player.to_lowercase() ==  "piedra" && second_player.to_lowercase() == "tijera" {
        status_play = 1;
    } else if first_player.to_lowercase() ==  "papel" && second_player.to_lowercase() == "tijera" {
        status_play = -1;
    } else if first_player.to_lowercase() ==  "papel" && second_player.to_lowercase() == "piedra" {
        status_play = 1;
    } 

    status_play
}


fn message_to_the_winner(first_player: String, second_player: String) -> i8 {
    let result_play: i8 = who_wins(first_player, second_player.clone());
    if result_play == 0 {
        println!("Empataste contra la maquina.");
    } else if result_play == 1 {
        println!("Le haz ganado a la maquina.");
    } else if result_play == -1 {
        println!("Haz perdido contra la maquina, ya que la maquina eligio {}.", second_player.clone())
    }
    result_play
}


fn get_good_value(message: &str,valid_words: Vec<&str>) -> String {
    loop {
        let user_choose: String = input_string(message);
        if valid_words.iter().any(|word| word.to_lowercase() == user_choose.to_lowercase()) {
            return user_choose;
        } else {
            println!("Ingresa un termino valido.")
        }
    }
}


fn start_game() -> i8{
    let options_to_choose: Vec<&str> = vec!["tijera", "piedra", "papel"];
    let generate_choose = rand::thread_rng().gen_range(0..=2);
    let computer_choose: String = String::from(options_to_choose[generate_choose]);
    // println!("I choose this option: {}", computer_choose);
    let user_choose: String = get_good_value("Ingresa tu opcion: ", options_to_choose);
    let this_happend: i8 = message_to_the_winner(user_choose, computer_choose);
    this_happend
}


fn game() -> () {
    let mut stadistic: DataParty = DataParty{won: 0, lose: 0, tie: 0, parties: 0};
    loop {
        let do_you_want_play = input_string("\n¿Quieres jugar S/si o N/no?:  ");
        if do_you_want_play.to_lowercase() == "s" || do_you_want_play.to_lowercase() == "si" {
            let result: i8 = start_game();
            change_status(&mut stadistic, result);

        } else if do_you_want_play.to_lowercase() == "n" || do_you_want_play.to_lowercase() == "no" {
            show_stadistic(stadistic);
            println!("Adios.");
            break;

        } else {
            println!("Por favor, usa una opcion valida.")
        }
    }
}


fn main() {
    let user_name: String = input_string("Ingresa tu nombre: ");
    println!("Bienvenido a jugar piedra, papel y tijera {user_name}: ");
    game()

}
