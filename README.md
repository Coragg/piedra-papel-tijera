# Papel, Piedra y Tijera

Esta es una practica del lenguaje de programación `Rust`, donde se llevara a cabo el juego de papel, piedra y tijera. 

Un juego simple, donde se siguen las siguientes reglas: 

* Si es tijera contra piedra, pierde la tijera
* Si es tijera contra papel, pierde el papel
* Si es papel contra piedra, pierde la piedra
* Hay empate si se elije el mismo. 

## Como ejecutar el programa

Primero debes tener el lenguaje de programación `Rust` en tu sistema operativo, para luego ejecutar el proyecto en la consola.

```sh
cargo run
```

Y despues a jugar. Es simple pero espero que te entretengas un rato. 




